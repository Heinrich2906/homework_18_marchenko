package com.company.controller;

import com.company.service.CustomersService;
import com.company.model.Customer;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.DateFormat;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Timestamp;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by heinr on 25.12.2016.
 */
public class CreateCustomerServlet extends HttpServlet {

    private CustomersService customerService = new CustomersService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/createCustomer")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        // Генерируем ответ и записываем его в response
        // Подробнее см. IndexServlet
        resp.setCharacterEncoding("utf-8");
        PrintWriter writer = resp.getWriter();
        List<Customer> customers = customerService.list();
        String content = generateCustomerHtml(customers);
        writer.write(content);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "create":

                String stringTime = params.get("birthdate")[0];

                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = (Date) formatter.parse(stringTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Timestamp timeStampDate = new Timestamp(date.getTime());

                customerService.create(
                        // Достаём значения полей формы из параметров запроса
                        params.get("firstname")[0], // Альтернатива req.getParameter("firstname")
                        params.get("lastname")[0],
                        timeStampDate,
                        params.get("address")[0],
                        params.get("city")[0],
                        params.get("passport")[0],
                        params.get("phone")[0]
                );
                break;

        }

        resp.sendRedirect("/customers?id=-1");
    }

    // Метод - генератор HTML кода страницы кустомеров
    private String generateCustomerHtml(List<Customer> customers) {

        return "<!DOCTYPE html> " +
                "<html> " +
                "<head> " +
                " <meta charset=\"utf-8\"> " +
                " <title>Kunden</title> " +
                " <link rel=\"stylesheet\" href=\"/css/style.css\"> " +
                "</head> " +
                "<body> " +
                "<div class=\"container\">" +
                " <img id=\"logo\" src=\"/img/Krauhe_Klar.png\"/> " +
                " <h1 id=\"Krahe\">Krähenbank</h1>" +
                "</div>" +
                " <ul> " +
                "  <li><a href=\"/customers?id=-1\">Kunden</a></li> " +
                "  <li><a href=\"/accounts?id=-1\">Konten</a></li> " +
                "  <li><a href=\"/transactions?id=-1\">Transaktionen</a></li> " +
                "  <ul class=\"home\"> " +
                "   <li><a href=\"/index\"><img src=\"../img/home.png\"></a></li> " +
                "  </ul> " +
                " </ul> " +
                " <div class=\"container\"> " +
                "  <div class=\"header\"> " +
                "Neukunde" +
                "  </div> " +

                "  <div id=\"form-container\"> " +
                "   <form action=\"createCustomer/create\" method=\"post\" id=\"customer-form\"> " +
                "    <label for=\"fname\">Vorname</label> " +
                "    <input class=\"input-box\" type=\"text\" id=\"fname\" name=\"firstname\" placeholder=\"First Name\"> " +
                " " +
                "    <label for=\"lname\">Nachname</label> " +
                "    <input class=\"input-box\" type=\"text\" id=\"lname\" name=\"lastname\" placeholder=\"Last Name\"> " +
                " " +
                "    <label for=\"birthdate\">Geburtstag</label> " +
                "    <input class=\"input-box\" type=\"date\" id=\"birthdate\" name=\"birthdate\" placeholder=\"Birthdate\"> " +
                " " +
                "    <label for=\"address\">Adresse</label> " +
                "    <input class=\"input-box\" type=\"text\" id=\"address\" name=\"address\" placeholder=\"Address\"> " +
                " " +
                "    <label for=\"city\">Stadt</label> " +
                "    <input class=\"input-box\" type=\"text\" id=\"city\" name=\"city\" placeholder=\"City\"> " +
                " " +
                "    <label for=\"passport\">Ausweis</label> " +
                "    <input class=\"input-box\" type=\"text\" id=\"passport\" name=\"passport\" placeholder=\"Passport\"> " +
                " " +
                "    <label for=\"phone\">Phone</label> " +
                "    <input class=\"input-box\" type=\"text\" id=\"phone\" name=\"phone\" placeholder=\"Phone\"> " +
                " " +
                "    <button type='submit' class=\"btn\">Erstellen</a> " +

                "  </div> " +
                " </div> " +
                "<script src=\"/javascript/script.js\"></script>" +
                "</body> " +
                "</html>";
    }
}

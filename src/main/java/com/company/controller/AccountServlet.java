package com.company.controller;

import com.company.model.Account;
import com.company.service.AccountsService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by heinr on 17.12.2016.
 */
public class AccountServlet extends HttpServlet {

    private AccountsService accountsService = new AccountsService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/accounts")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }
        Long customerId = Long.parseLong(req.getParameter("id"));
        // Генерируем ответ и записываем его в response
        // Подробнее см. IndexServlet
        resp.setCharacterEncoding("utf-8");
        PrintWriter writer = resp.getWriter();
        List<Account> accounts = accountsService.list(customerId);
        String content = generateAccountHtml(accounts);
        writer.write(content);
    }

    // Обработчик POST запросов на
    // http://localhost:8080/customers/create
    // и
    // http://localhost:8080/customers/delete
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "create":
//                customerService.create(
//                        // Достаём значения полей формы из параметров запроса
//                        params.get("firstname")[0], // Альтернатива req.getParameter("firstname")
//                        params.get("lastname")[0],
//                        params.get("birthdate")[0],
//                        params.get("address")[0],
//                        params.get("city")[0],
//                        params.get("passport")[0],
//                        params.get("phone")[0]
//                );
                break;
            case "delete":
                // Достаем поле id из формы удаления кастомера и удаляем соответствующую запись через сервис
                try {
                    Long accountNumber = Long.parseLong(params.get("id")[0]);
                    accountsService.delete(accountNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

        resp.sendRedirect("/customers");
    }

    // Метод - генератор HTML кода страницы accounts
    private String generateAccountHtml(List<Account> accounts) {
        StringBuilder sb = new StringBuilder();
        for (Account account : accounts)
            sb.append(createAccountRow(account));
        String userRows = sb.toString();

        return "<!DOCTYPE html> " +
                "<html> " +
                "<head> " +
                " <meta charset=\"utf-8\"> " +
                " <title>Krähenbank</title> " +
                " <link rel=\"stylesheet\" href=\"/css/style.css\"> " +
                "</head> " +
                "<div class=\"container\">" +
                " <img id=\"logo\" src=\"/img/Krauhe_Klar.png\"/> " +
                " <h1 id=\"Krahe\">Krähenbank</h1>" +
                "</div>" +
                " <ul> " +
                "<body> " +
                " <ul> " +
                "  <li><a href=\"/customers\">Kunden</a></li> " +
                "  <li><a class=\"active\" href=\"/accounts?id=-1\">Konten</a></li> " +
                "  <li><a href=\"/transactions?id=-1\">Transaktionen</a></li> " +
                "  <ul class=\"home\"> " +
                "   <li><a href=\"/index\"><img src=\"../img/home.png\"></a></li> " +
                "  </ul> " +
                " </ul> " +
                " <div class=\"container\"> " +
                "  <div class=\"header\"> " +
                "Konten" +
                "  </div> " +
                "  <div class=\"table\"> " +
                "   <table> " +
                "    <thead> " +
                "     <tr> " +
                "      <th>Accountnumber</th> " +
                "      <th>Balance</th> " +
                "      <th>Erstellungsdatum</th> " +
                "      <th>Währung</th> " +
                "      <th>Ist blockiert</th> " +
                "      <th>Kundennummer</th> " +
                "      <th></th> " +
                "     </tr> " +
                "    </thead> " +
                "    <tbody> " +

                userRows +

                "    </tbody> " +
                "   </table> " +
                "  </div> " +
                " </div> " +
                "<script src=\"/javascript/script.js\"></script>" +
                "</body> " +
                "</html>";
    }

    // Метод - генератор HTML кода строки (элемент tr) для таблицы accounts
    private String createAccountRow(Account account) {

        Long accountId = account.getAccountNumber();

        return "     <tr> " +
                "      <td><a href=\"/transactions?id=" + accountId.toString() + "\">" + accountId + "</a></td> " +
                "      <td>" + account.getBalance() + "</td> " +
                "      <td>" + account.getCreationDate() + "</td> " +
                "      <td>" + account.getCurrency() + "</td> " +
                "      <td>" + account.isBlocked() + "</td> " +
                "      <td>" + account.getCustomerId() + "</td> " +
                "      <td>" +
                "<form action=\"accounts/delete\" method=\"post\">" +
                "<input type=\"hidden\" name=\"id\" value=\"" + accountId + "\"/>" +
                "<button type=\"submit\" class=\"customer-delete\"></button>" +
                "</form>" +
                "      </td>" +
                "     </tr> ";
    }
}

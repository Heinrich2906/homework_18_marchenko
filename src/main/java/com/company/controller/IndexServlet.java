package com.company.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// Класс сервлета наследуется от javax.servlet.http.HttpServlet
public class IndexServlet extends HttpServlet {

    // doGet обрабатывает GET запросы на http://localhost:8080/
    // см. файл web.xml
    // req - объект-запрос браузера
    // resp - объект ответ сервера
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        resp.setCharacterEncoding("utf-8");

        // Получаем у объекта response (ответ сервера) поток вывода
        PrintWriter writer = resp.getWriter();

        // Записываем в поток HTML. Весь записанный текст будет отображён в браузере
        writer.print("<html>" +
                         "<head> " +
                            " <meta charset=\"utf-8\"> " +
                            " <title>Krähenbank</title> " +
                            " <link rel=\"stylesheet\" href=\"/css/style.css\"> " +
                        "</head> " +
                    "<body class=\"container\">" +
                    "<div>" +
                        " <img id=\"logo\" src=\"/img/Krauhe_Klar.png\"/> " +
                        " <h1 id=\"Krahe\">Krähenbank</h1>" +
                    "</div>" +

        " <ul> " +
                "  <li><a href=\"/customers\">Kunden</a></li> " +
                "  <li><a href=\"/accounts?id=-1\">Konten</a></li> " +
                "  <li><a href=\"/transactions?id=-1\">Transaktionen</a></li> " +
                " </ul> "+
                "<span>" +
                "<h2>Willkomen bei der Bank!</h2>" +
                "</span>");
        writer.print("</body>");
        writer.print("</html>");
    }

}

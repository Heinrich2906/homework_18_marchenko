package com.company.dao;

import com.company.util.GetConnection;
import com.company.model.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;


/**
 * Created by heinr on 27.11.2016.
 */
public class TransactionDaoImpl implements TransactionDao {

    public Transaction get(Long id) throws Exception {

        Transaction transaction = null;
        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM transaction WHERE id = ?");

            //2.1. Передача параметра
            statement.setLong(1, id);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();

                transaction = new Transaction(resultSet.getLong("id"),
                        resultSet.getBigDecimal("amount"),
                        resultSet.getTimestamp("date"),
                        resultSet.getString("operationType"),
                        resultSet.getLong("accountNumber"));
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return transaction;
    }

    public Long save(Transaction transaction) {

        Connection connection = null;

        boolean flag = false;
        Long result = 0L;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT FIRST 1 id FROM transactions WHERE id = ?");

            //2.1. Передача параметра
            statement.setLong(1, transaction.getId());

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();
                flag = true;

                // Шаг 5.1. Закрыть ResultSet
                resultSet.close();

                this.update(transaction);

                result = transaction.getId();
            }

            if (!flag) {

                statement = connection.prepareStatement("INSERT INTO transactions (amount, date, operationType, accountNumber) VALUES(?,?,?,?)");

                statement.setBigDecimal(1, transaction.getAmount());
                statement.setTimestamp(2, transaction.getDate());
                statement.setString(3, transaction.getOperationType());
                statement.setLong(4, transaction.getAccountNumber());

                int blank = statement.executeUpdate();

                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    // Получаем поле id
                    result = resultSet.getLong("id");
                }
            }

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void update(Transaction transaction) {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("UPDATE accounts SET amount = ?, date = ?, operationType = ?, accountNumber = ? where id = ?");

            //2.1. Передача параметра
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            statement.setLong(5, transaction.getId());

            //3. Выполнение запроса
            int blank = statement.executeUpdate();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Transaction> list(Long id) {

        Connection connection = null;
        List<Transaction> accountList = new ArrayList<>();

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement();

            ResultSet resultSet = null;

            //3. Выполнение запроса
            if (id == -1L) {
                resultSet = statement.executeQuery("SELECT * FROM transactions");
            } else {
                resultSet = statement.executeQuery("SELECT * FROM transactions WHERE accountNumber = " + id.toString());
            }

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();

                Transaction transaction = new Transaction(resultSet.getLong("id"),
                        resultSet.getBigDecimal("amount"),
                        resultSet.getTimestamp("date"),
                        resultSet.getString("operationType"),
                        resultSet.getLong("accountNumber"));

                accountList.add(transaction);

            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return accountList;
    }

    public void delete(Long id) {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("DELETE FROM transactions WHERE id = ?");

            //2.1. Передача параметра
            statement.setLong(1, id);

            //3. Выполнение запроса
            int blank = statement.executeUpdate();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteByAccount(Long accountId) {

        Connection connection = null;
        boolean result = false;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            result = deleteByAccountCommon(accountId, connection);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public boolean deleteByAccountCommon(Long accountId, Connection connection) throws SQLException {

        PreparedStatement statement = null;

        try {
            //2. Создание запроса
            statement = connection.prepareStatement("DELETE FROM transactions WHERE accountNumber = ?");

            //2.1. Передача параметра
            statement.setLong(1, accountId);

            //3. Выполнение запроса
            int blank = statement.executeUpdate();

            return true;

        } catch (SQLException e) {
            return false;
        } finally {
            // Шаг 5.2. Закрыть Statement
            if (statement != null) {
                statement.close();
            }

        }

    }
}

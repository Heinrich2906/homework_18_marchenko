package com.company.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by heinr on 27.11.2016.
 */
public class Transaction {

    private Long id;
    private BigDecimal amount;
    private java.sql.Timestamp date;
    private String operationType;
    private Long accountNumber;

    public Transaction(Long id, BigDecimal amount, Timestamp date, String operationType, Long accountNumber) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.operationType = operationType;
        this.accountNumber = accountNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String oterationType) {
        this.operationType = oterationType;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", Date=" + date +
                ", oterationType='" + operationType + '\'' +
                ", accountNumber=" + accountNumber +
                '}';
    }
}

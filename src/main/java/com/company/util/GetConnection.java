package com.company.util;

import java.sql.DriverManager;
import java.util.Properties;
import java.sql.Connection;

/**
 * Created by heinr on 30.11.2016.
 */
public class GetConnection {

    public static Connection getConnection() throws Exception {

        Connection connection = null;

        Properties info = new Properties();
        info.put("user", "postgres");
        info.put("password", "admin");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", info);

        return connection;

    }
}

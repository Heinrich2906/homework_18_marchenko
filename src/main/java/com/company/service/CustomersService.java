package com.company.service;

import com.company.dao.CustomerDaoImpl;
import com.company.dao.CustomerDao;
import com.company.model.Customer;
import com.company.model.Account;

import java.time.LocalDateTime;
import java.math.BigDecimal;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 27.11.2016.
 */
public class CustomersService {

    CustomerDao customerDao = new CustomerDaoImpl();

    public Customer get(Long id) throws Exception {

        Customer customer = customerDao.get(id);

        return customer;
    }

    Long save(Customer customer) {

        boolean flag = false;

        if (customer.getId() == null) flag = true;

        Long result = customerDao.save(customer);

        if (flag) {

            Account account = new Account(null,
                    new BigDecimal("0"),
                    Timestamp.valueOf(LocalDateTime.now()),
                    "UAH",
                    false,
                    customer.getId());

            AccountsService accountsService = new AccountsService();
            accountsService.save(account);
        }

        return result;
    }

    void update(Customer customer) {
        customerDao.update(customer);
    }

    public void delete(Long customerId) throws Exception {
        customerDao.delete(customerId);
    }

    //Вариант 1, день рождения - СТРОКА
    public void create(String firstname, String lastname, String birthdate, String address,
                       String city, String passport, String phone) {

        Customer customer = new Customer(null, firstname, lastname,Timestamp.valueOf(birthdate),address,city,passport,phone);
        CustomersService customersService = new CustomersService();
        Long id = customersService.save(customer);
        }

    //Вариант 2, день рождения - TimeStamp
    public void create(String firstname, String lastname, Timestamp birthdate, String address,
                       String city, String passport, String phone) {

        Customer customer = new Customer(null, firstname, lastname,birthdate,address,city,passport,phone);
        Long id = this.save(customer);
    }

    public List<Customer> list() {

        List<Customer> list = new ArrayList<>();
        list = customerDao.list();

        return list;
    }

}